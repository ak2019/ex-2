
// BACK TO TOP BUTTON /////////////////////////////////////////https://html-online.com/articles/dynamic-scroll-back-top-page-button-javascript/
$(window).scroll(function() {
    var height = $(window).scrollTop();
    if (height > 4000) {
        $('#back2Top').fadeIn();
    } else {
        $('#back2Top').fadeOut();
    }
});
$(document).ready(function() {
    $("#back2Top").click(function(event) {
        event.preventDefault();
        $("html, body").animate({ scrollTop: 0 }, "slow");
        return false;
    });

});

// PROGRESS BAR ON TOP OF THE PAGE ///// ///////////////////////https://eloquentjavascript.net/15_event.html
  var bar = document.querySelector("#progress");
  window.addEventListener("scroll", () => {
    let max = document.body.scrollHeight - innerHeight;
    bar.style.width = `${(pageYOffset / max) * 100}%`;
  });


// MOVE DOT ON MENU //////////////////////////////////////
window.onscroll = () => {

  const li1 = document.getElementById('nav1');
  if(this.scrollY >= 0 && this.scrollY < 300) li1.className = 'menu-yellow'; else li1.className = 'menu';

  const li2 = document.getElementById('nav2');
  if(this.scrollY >= 300 && this.scrollY < 1115) li2.className = 'menu-yellow'; else li2.className = 'menu';

  const li3 = document.getElementById('nav3');
  if(this.scrollY >= 1115 && this.scrollY < 1930) li3.className = 'menu-yellow'; else li3.className = 'menu';

  const li4 = document.getElementById('nav4');
  if(this.scrollY >= 1930 && this.scrollY < 2745) li4.className = 'menu-yellow'; else li4.className = 'menu';

  const li5 = document.getElementById('nav5');
  if(this.scrollY >= 2745 && this.scrollY < 3560) li5.className = 'menu-yellow'; else li5.className = 'menu';

  const li6 = document.getElementById('nav6');
  if(this.scrollY > 3560 && this.scrollY < 4375) li6.className = 'menu-yellow'; else li6.className = 'menu';            

  const li7 = document.getElementById('nav7');
  if(this.scrollY > 4375) li7.className = 'menu-yellow'; else li7.className = 'menu';  

};


/////////////////////////////////////////////////////////////////SCROLL TO LINK - COPIED.
/////////////////////////////////////////////////////////////////http://javascriptkit.com/javatutors/scrolling-html-bookmark-javascript.shtml
let anchorlinks = document.querySelectorAll('a[href^="#"]')
 
for (let item of anchorlinks) {  
    item.addEventListener('click', (e)=> {
        let hashval = item.getAttribute('href')
        let target = document.querySelector(hashval)
        target.scrollIntoView({
            behavior: 'smooth',
            block: 'start'
        })
        history.pushState(null, null, hashval)
        e.preventDefault()
    })
}

////////////////////////////////////////////////////////////////



